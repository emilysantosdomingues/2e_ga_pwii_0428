import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AutoresService } from './services/autores/autores.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DetalhesLivroComponent } from './components/detalhes-livro/detalhes-livro.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LivrosService } from './services/livros/livros.service';

@NgModule({
  declarations: [
    AppComponent,
    DetalhesLivroComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    LivrosService,
    AutoresService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
