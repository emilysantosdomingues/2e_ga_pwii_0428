import { Component, OnInit } from '@angular/core';
import { Autor } from 'src/app/models/Autor';
import { AutoresService } from 'src/app/services/autores/autores.service';
import { LivrosService } from 'src/app/services/livros/livros.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  autores: Autor[]

  constructor(
    private livrosService: LivrosService,
    private autoresService: AutoresService
  ) {
    this.autores = []
   }

  ngOnInit(): void {
    this.autoresService.buscarTodosOsAutores().subscribe({
      next: (resposta) => {
        this.autores = resposta.results
        console.log(this.autores)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
    /*
    this.livrosService.buscarLivros().subscribe(
      (livros) => {
        console.log(livros)
      },
      (erro) => {
        console.error(erro)
      }
    )*/
  }

}
