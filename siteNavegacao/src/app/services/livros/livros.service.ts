import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LivrosService {
  private readonly URL_A = "https://3000-amandamello-2egaapi0810-4vj8984cm29.ws-us77.gitpod.io/"
  private readonly URL_E = "https://3000-amandamello-2egaapi0810-8xz0fu351wu.ws-us73.gitpod.io/"
  private readonly URL_I = "https://3000-amandamello-2egaapi0810-ugltvq4llnj.ws-us74.gitpod.io/"
  private readonly URL = this.URL_E

  constructor(
    private http: HttpClient
  ) { }

  buscarLivros(): Observable<any> {
    return this.http.get<any>(`${this.URL}livros`)
  }
}
