import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutoresService {
  private readonly URL_A = "https://3000-amandamello-2egaapi0810-4vj8984cm29.ws-us77.gitpod.io/"
  private readonly URL_E = "https://3000-amandamello-2egaapi0810-8xz0fu351wu.ws-us73.gitpod.io/"
  private readonly URL_I = "https://3000-amandamello-2egaapi0810-ugltvq4llnj.ws-us77.gitpod.io/"
  private readonly URL = this.URL_I


  constructor(
    private http: HttpClient
  ) { }

  buscarTodosOsAutores(): Observable<any> {
    return this.http.get<any>(`${this.URL}autores`)
  }
}
